using System.Collections;
using System.Collections.Generic;

public static class Config {
    // make these lists instead, and do a List contains "somestring"
    // this makes configuration easier
    public static Dictionary<string, string> dataCollectionTypes = new Dictionary<string, string>() {
        {"villagerNames", "singleFile"},
        {"buildings", "listOfDirs"},
        {"research", "listOfDirs"},
        {"events", "listOfFiles"},
        {"stats", "listOfFiles"}
    };

    public static Dictionary<string, List<string>> dataLists = new Dictionary<string, List<string>>();
    public static Dictionary<string, string> data = new Dictionary<string, string>() {
        {"villagerNames", "data/villageCenter/villagerNames.txt"},
        {"buildings", "data/building"},
        {"research", "data/research"},
        {"events", "data/events"},
        {"stats", "data/villageCenter/stats"},
    };
    public static Dictionary<string, List<string>> expectedFiles = new Dictionary<string, List<string>>() {
        {"buildings", new List<string>(new string[] {"consume.json", "produce.json", "construct.json"})},
        {"research", new List<string>(new string[] {"consume.json", "produce.json", "construct.json"})}
    };
    public static List<string> stats = new List<string>(
        new string[] {
            "clothing.json",
            "energy.json",
            "tool.json",
            "warmth.json"
        }
    );
    public static List<string> events = new List<string>(
        new string[] {
            "acidRain.json",
            "caveIn.json",
            "famine.json",
            "forestFire.json",
            "maintenance.json",
            "monsoon.json",
            "mutantMoths.json",
            "perfectWeather.json",
            "scarceResources.json",
            "snowStorm.json"
        }
    );
    public static List<string> buildings = new List<string>(
        new string[] {
            "chandler",
            "forester",
            "gatherer",
            "hunter",
            "miner",
            "shipwright",
            "smith",
            "tailor",
            "woodcutter"
        }
    );
    public static List<string> research = new List<string>(
        new string[] {
            "automatons"
        }
    );
    public static void Init() {
        dataLists.Add("stats", stats);
        dataLists.Add("buildings", buildings);
        dataLists.Add("research", research);
        dataLists.Add("events", events);
    }
}
